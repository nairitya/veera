function initialize() {
    document.getElementById("tags").selectedIndex = "-1";
    document.getElementById("gender").style.visibility = "hidden";
    document.getElementById("operator").style.visibility = "hidden";
    first = 0;
    map = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom: 5,
        center: new google.maps.LatLng(locations[0][0], locations[0][1]),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var i, icon;

    for (i = 0; i < (locations.length)-1; i++) {  

        (function(i, total){
            marker[i] = new google.maps.Marker({
                position: new google.maps.LatLng(total[i][0], total[i][1]),
                map: map,
                animation: google.maps.Animation.DROP,
                title: i.toString()
            });

            google.maps.event.addListener(marker[i], 'click', (function(marker, i) {
                return function() {
                    console.log("Pressed me !!");
                    document.getElementById("markers").selectedIndex = String(i);
                }
            })(marker[i], i));
        })(i, locations);
    }
}

function markerf(mar, ques){
    first = parseInt(mar.value);
}

function tagfunc(mar, ques){

    switch(mar.value){
        case "0":
            // document.getElementById("operator").style.visibility = "hidden";
            // document.getElementById("gender").style.visibility = "visible";
            for(var i=0;i<gender.length;i++){
                switch(gender[i]){
                    case "Male":
                        marker[i].setIcon(urls[1]);
                        break;
                    case "Female":
                        marker[i].setIcon(urls[2]);
                        break;
                    default:
                        console.log("Nope 1");
                        break;
                }
            }
            break;
        case "1":
            // document.getElementById("operator").style.visibility = "visible";
            // document.getElementById("gender").style.visibility = "hidden";
            for(var i=0;i<operator.length;i++){
                switch(operator[i]){
                    case "Airtel":
                        marker[i].setIcon(urls[1]);
                        break;
                    case "Idea":
                        marker[i].setIcon(urls[2]);
                        break;
                    case "Vodafone":
                        marker[i].setIcon(urls[3]);
                        break;
                    default:
                        console.log("Nope 2");
                        break;
                }
            }
            break;
        default:
        console.log(mar.value);
            console.log("Nope 3");
            break;
    }
        
}

function circleOnMap(){
    indexes = [];
    for(var i=0;i<locations.length-1;i++){
        marker[i].setMap(null);
    }
    var main_loc = document.getElementById("markers").value;
    main_loc = parseInt(main_loc);

    for(var i=0;i<locations.length-1;i++){
        getDistance(locations[i], locations[main_loc],i);
    }
}

function getDistance(a, b, i){
    var origin1 = new google.maps.LatLng(a[0], a[1]);
    var destinationA = new google.maps.LatLng(b[0], b[1]);
    // var destinationB = new google.maps.LatLng(50.087692, 14.421150);

    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(
    {
        origins: [origin1],
        destinations: [destinationA],
        travelMode: google.maps.TravelMode.DRIVING
    },function(response, status){ callback(response, status, i);});
}

function callback(response, status, i) {
    // console.log(i);
    if (status == google.maps.DistanceMatrixStatus.OK) {
        var origins = response.originAddresses;
        var destinations = response.destinationAddresses;

        var element = response.rows[0].elements[0];
        var distance = element.distance.text;
        // console.log(distance);

        var radiusOfCircle = document.getElementById("radius").value;
        radiusOfCircle = parseInt(radiusOfCircle);

        if(parseInt(distance.replace(",","")) <= radiusOfCircle){
            indexes.push(i)
        }
    }

    if(i == locations.length-2){
        // DrawIt();
        initialize_drawer();
    }
}

function DrawIt(){
    console.log(indexes);
    console.log(locations[parseInt(first)][0]);
    var mapOptions = {
        zoom: 4,
        center: new google.maps.LatLng(locations[parseInt(first)][0], locations[parseInt(first)][1]),
        mapTypeId: google.maps.MapTypeId.TERRAIN
    };

    var radiusOfCircle = document.getElementById("radius").value;
        radiusOfCircle = parseInt(radiusOfCircle);

    var populationOptions = {
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        map: map,
        center: mapOptions.center,
        radius: radiusOfCircle*1000
    };
    // Add the circle for this city to the map.
    if(typeof cityCircle != 'undefined')
    cityCircle.setMap(null);

    cityCircle = new google.maps.Circle(populationOptions);
    cityCircle.setMap(map);
}
