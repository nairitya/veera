
        
        // Loading Google Visualization API Reference
google.load( 'visualization', '1', { packages:['corechart'] });


// Creating chart as marker_drawer
function Chartmarker_drawer( options ) {
    this.setValues( options );
    
    this.$inner = $('<div>').css({
        position: 'relative',
        left: '-50%', top: '-50%',
        width: options.width,
        height: options.height,
        fontSize: '1px',
        lineHeight: '1px',
        backgroundColor: 'transparent',
        cursor: 'default'
    });

    this.$div = $('<div>')
        .append( this.$inner )
        .css({
            position: 'absolute',
            display: 'none'
        });
};

Chartmarker_drawer.prototype = new google.maps.OverlayView;

Chartmarker_drawer.prototype.onAdd = function() {
    $( this.getPanes().overlayMouseTarget ).append( this.$div );
};

Chartmarker_drawer.prototype.onRemove = function() {
    this.$div.remove();
};

Chartmarker_drawer.prototype.draw = function() {
    var marker_drawer = this;
    var projection = this.getProjection();
    var position = projection.fromLatLngToDivPixel( this.get('position') );

    this.$div.css({
        left: position.x,
        top: position.y,
        display: 'block'
    })

    this.$inner
        // .html( '<img src="' + this.get('image') + '"/>' )
        .click( function( event ) {
            var events = marker_drawer.get('events');
            events && events.click( event );
        });
        
    this.chart = new google.visualization.PieChart( this.$inner[0] );
    this.chart.draw( this.get('chartData'), this.get('chartOptions') );
};

// Initializing map with data for pie chart

function initialize_drawer() {
    first = parseInt(first);
    var latLng = new google.maps.LatLng(locations[first][0], locations[first][1]);

    map = new google.maps.Map( $('#map-canvas')[0], {
        zoom: 11,
        center: latLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    
    if(document.getElementById("tags").value == "0"){
        /* It is gender */
        var male =0, female = 0;
        for(var i=0; i<indexes.length; i++){
            if(gender[indexes[i]] == "Male")
                male++;
            else
                female++;
        }

        var data = google.visualization.arrayToDataTable([
            [ 'Gender', 'Total' ],
            [ 'Male', male ],
            [ 'Female', female ]
        ]);

    } else if(document.getElementById("tags").value == "1"){
        /*It is Operator */
        var idea =0, vodafone = 0, airtel =0;
        for(var i=0; i<indexes.length; i++){
            if(operator[indexes[i]] == "Idea")
                idea++;
            else if(operator[indexes[i]] == "Airtel")
                airtel++;
            else
                vodafone++;
        }

        var data = google.visualization.arrayToDataTable([
            [ 'Operator', 'Total' ],
            [ 'Idea', idea ],
            [ 'Airtel', airtel ],
            [ 'Vodafone', vodafone]
        ]);
    } else {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     11],
          ['Eat',      2],
          ['Commute',  2],
          ['Watch TV', 2],
          ['Sleep',    7]
        ]);
    }
    
    var options = {
        fontSize: 12,
        backgroundColor: 'transparent',
        legend: 'none'
    };
    
    var marker_drawer = new Chartmarker_drawer({
        map: map,
        position: latLng,
        width: '100px',
        height: '100px',
        chartData: data,
        chartOptions: options,
        events: {
            click: function( event ) {
                // alert( 'Latitude: ' + marker_drawer.position.hb + 'and Longitude: ' + marker_drawer.position.ib ); //Clicked marker_drawer lat lng
            }
        }
    });
};
