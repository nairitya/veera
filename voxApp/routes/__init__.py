import os, csv, re
from voxApp import app
from flask import Flask, jsonify, request, render_template
from flask import make_response, url_for, redirect
from werkzeug import secure_filename
import jsontree

ALLOWED_EXTENSIONS = set(['csv'])

@app.route('/')

def home():
	return render_template("home.html")


"""
direct split => 0 to three => Gender,Operator,State,City
split with " => [1] => Brands Like
Use regex :) => Lat lang
"""

@app.route('/map', methods=['POST'])
def mapp():
	file = request.files['file']
	
	coordinates = []
	extra = []
	brands = []
	q = []
	status = []

	if file and allowed_file(file.filename):
		# filename = secure_filename(file.filename)
		file.save(os.path.join(app.config['UPLOAD_FOLDER'], "sample.csv"))

	with open(os.path.join(app.config['UPLOAD_FOLDER'], "sample.csv"), 'rb') as f:
		reader = csv.DictReader(f)
		for row in reader:
			brands.append(row["Brands Like"])

			temp = []
			temp.append(row["Latitude"])
			temp.append(row["Longitude"])
			coordinates.append(temp)

			ex = []
			ex.append(row["Gender"])
			ex.append(row["Operator"])
			ex.append(row["State"])
			ex.append(row["City"]) #Gender,Operator,State,City
			extra.append(ex)

			ques = row["Question"].split(",")
			# print ques
			qq = []
			for t in ques:
				qq.append(t)
			qq.pop()
			q.append(qq)

			sts = row["Status"].split(',')
			qq = []
			for t in sts:
				qq.append(t)
			qq.pop()
			status.append(qq)


	# q = [['Q11', 'Q12', 'Q13'], ['Q21', 'Q22', 'Q23'], ['Q31', 'Q32', 'Q33'], ['Q41', 'Q42', 'Q43'], ['Q51','Q52','Q53'], ['Q61','Q62','Q63'], ['Q71', 'Q72', 'Q73']]

	# status = [['unsolved', 'solved', 'solved'], ['unsolved', 'solved', 'solved'], ['solved', 'solved', 'unsolved'], ['solved', 'solved', 'solved'], ['solved', 'solved', 'unsolved'], ['solved','unsolved', 'solved'], ['Q71', 'solved', 'solved']]
	
	# print brands
	return render_template("map.html", data=coordinates, extra=extra, q=q, s=status, b=brands)

def allowed_file(filename):
	return '.' in filename and \
		filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/map2', methods=['POST'])

def new_route():
	file = request.files['file']
	if file and allowed_file(file.filename):
		file.save(os.path.join(app.config['UPLOAD_FOLDER'], "sample.csv"))

	ini = []
	with open(os.path.join(app.config['UPLOAD_FOLDER'], "sample.csv"), 'rb') as f:
		initial_detail = csv.reader(f, delimiter=',', quotechar='|')
		for row in initial_detail:
			for a in row:
				ini.append(a)
			break
	# print ini

	coordinates = []
	city_detail = []
	gender = []
	operator = []
	with open(os.path.join(app.config['UPLOAD_FOLDER'], "sample.csv"), 'rb') as f:
		reader = csv.DictReader(f)
		for row in reader:
			temp = []
			temp.append(row["Latitude"])
			temp.append(row["Longitude"])
			coordinates.append(temp)

			temp = []
			temp.append(row["City"])
			temp.append(row["State"])
			city_detail.append(temp)

			gender.append(row["Gender"])
			operator.append(row["Operator"])

	#Hardcoding this init
	ini = ['Gender', 'Operator']
	data = jsontree.jsontree()
	data.tags = ini
	data.coordinates = coordinates
	data.city = city_detail
	data.operator = operator
	data.gender = gender

	return render_template('map2.html', data=data)
