import os
from flask import Flask, jsonify, request, render_template

tempdir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')
UPLOAD_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'uploads')

app = Flask("voxApp", template_folder=tempdir)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

from voxApp import routes