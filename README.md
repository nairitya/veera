# Veera

Veera is a data analyser powered by Google Map Api's.

  - Upload the csv
  - see the markers according to selection
  - Define a circle and see a piechart

Right now it is static and hard-coded for operators and gender like Airtel, Idea and Vodafone only. Modifications that can be done here are:


  - Dynamic selection of operators from a list.
  - Dynamic selection of tags from the csv.
  - More features in UI.
  - Write Tests.


### Version
1.0.0

### Tech

Veera uses a number of API's and open source project to work properly:

* [Flask] - Microframework for Python
* [Google Map] - Google Maps JavaScript API v3
* [etechpulse] - great UI boilerplate for modern web apps
* [Google Interactive Chart] - Display live data on your site
* [jQuery] - duh

### Installation

You need python installed:

```sh
$ git clone git@bitbucket.org:nairitya/veera.git
$ cd veera
$ pip install -r requirements.txt
$ python app.py
```

License
----
MIT

[etechpulse]:http://www.etechpulse.com/2013/07/how-to-create-chart-on-google-maps.html
[Google Interactive Chart]:https://developers.google.com/chart/
[jQuery]:http://jquery.com/
[Flask]:http://flask.pocoo.org/
[Google Map]:https://developers.google.com/maps/

